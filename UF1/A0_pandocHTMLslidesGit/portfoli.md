# UF2. Portfoli (Part I)
## git, markdown, pandoc
---

<ins>S'avaluarà tant la `qualitat` del **contingut** com de la **presentació**.</ins>


Es demana:

Crear un _directori_ `git`, on a dintre hi tindreu els següents fitxers.

## Fitxers/directoris que ha d'incloure:

* README.md
* pandoc.md
* Directori aux
* Directori slides
  - git.pdf
  - git.html

# Fitxer `README.md`

<ins>Contingut</ins>:

- Organització les zones de treball i conceptes bàsics
- Creació inicial
- Sincronització local/remot
- Ús de claus per facilitar la pujada
- Operacions amb el remot (add, delete)
- Creació de branques, canvi entre branques, fusions i esborrats
- Desfer canvis, en els casos més habituals:
    - Fitxers no seguits
    - Desfer canvis afegits a l'index
    - Desfer un commit
- Qualsevol punt que considereu interessant

# Fitxer `pandoc.md`

- Descripció en detall de les opcions que has utilitzat per generar HTML slides i PDF slides amb els formats respectius slidy i beamer així com de l ús de les variables de metadades.

# Directori `aux`

Tot allò necessari per implementar el que es demana

# Directori `slides`

Contindrà els fitxers slides generats amb pandoc amb els formats slidy i beamer


**`IMPORTANT`**
**Es comprovarà l'ús del contingut per part de l'alumne així com l'assoliment del contingut de la presentació**
