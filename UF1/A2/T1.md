Dissenyar nous DTDs
===================

MP4UF1A2T1

Introducció a la descripció de tipus de documents

Definició de Tipus de Document
------------------------------

Un document XML és vàlid si, a més a més de ser ben format, la seva
estructura respecta les restriccions establertes en el seu DTD
(*Document Type Definition*).

-   La declaració de tipus de document (`<!DOCTYPE nom-element-arrel…>`)
    estableix quin és el DTD del document XML.
-   El DTD pot residir dins el document XML o en un fitxer separat.
-   L’ordre `xmllint`(1) por ser usada per validar documents XML
    (opcions `--valid` i `--noout`).

Declaracions d’element i d’atribut
----------------------------------

Aquestes són les úniques dos declaracions disponibles per definir la
gramàtica d’un vocabulari XML.

**Nota**: les declaracions d’entitat i de tipus de document, amb la que
es completen les declaracions existents en els DTDs, no serveixen per
definir la gramàtica del document.

-   La declaració d’element; (`<!ELEMENT nom-element…>`).
-   La declaració d’atribut; (`<!ATTLIST nom-element nom-atribut…>`).
-   Podem tenir més d’una declaració de llista d’atributs per un mateix
    element .

Enllaços recomanats
-------------------

-   [XML Syntax Quick
    Reference](http://www.mulberrytech.com/quickref/XMLquickref.pdf)
    ([local](../A1/aux/XMLquickref.pdf))
-   [DTD Tutorial](http://www.w3schools.com/xml/xml_dtd_intro.asp)
-   [XML Design Patterns](http://www.xmlpatterns.com/)

L’XML Syntax Quick Reference serà presentada en la seva totalitat a
classe.

Pràctiques
----------

-   Practica els tutorials citats en la llista d’enllaços.
-   Fes un document XML, amb el seu corresponent DTD, on es presentin
    els 4 casos possibles de model de contigut per a elements: buit,
    elements fills, text (`#PCDATA`), i *mixed* (elements i text).
-   Crea un document XML descrivint un menú de restaurant (i el seu DTD)
    per mostrar el patró de disseny Common Attributes.

