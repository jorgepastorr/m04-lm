Editar documents XML ben formats
================================

MP4UF1A1T2

Us d’editors de propòsit general amb documents XML

XML ben format
--------------

-   Declaració XML: `<?xml version="1.0" encoding="utf-8"?>`
-   Un sol element arrel
-   Aniuament elements: `<p><b>...</b>...</p>`
-   Sensible a majúscules/minúscules
-   Valors atributs entre cometes: `href="..."`
-   Entitats predefinides: `&amp;` `&lt;` `&gt;` `&apos;` `&quot;`

Els comentaris en l’XML tenen aquest aspecte: `<!-- ... -->` (no poden
contenir en mig dos guions seguits)

Editor visual
-------------

-   Tots els editors moderns implementen les dreceres de teclat CUA (IBM
    Common User Access).
-   Implementen cerca i substitució utilitzant expressions regulars.
-   Gestionen diferents codificacions de caràcters o finals de línia.
-   Permeten editar múltiples fitxers simultàniament.
-   Ofereixen sintaxi colorejada.
-   Agrupen, per plegar/desplegar, els blocs de codi segons
    el llenguatge.
-   Són extensibles amb plugins.

Altres formats de text estructurat
----------------------------------

-   Camps delimitats per un caràcter, i registres delimitats per salt de
    línia (`/etc/passwd`).
-   [INI](http://en.wikipedia.org/wiki/INI_file), en diferents variants
    (`/etc/systemd/system/*.service`).
-   [CSV](http://en.wikipedia.org/wiki/Comma-separated_values) (opció en
    usar PostgreSQL `COPY`).
-   [JSON](http://en.wikipedia.org/wiki/JSON), important
    competidor d’XML.
-   [YAML](http://en.wikipedia.org/wiki/YAML), competidor d’XML
    compatible amb JSON.

Enllaços recomanats
-------------------

-   [Wikipedia: IBM Common User
    Access](http://en.wikipedia.org/wiki/IBM_Common_User_Access)
-   [Wikipedia:
    Clipboard](http://en.wikipedia.org/wiki/Clipboard_%28software%29)
-   [XML Syntax Quick
    Reference](http://www.mulberrytech.com/quickref/XMLquickref.pdf)
    ([local](aux/XMLquickref.pdf))
-   [fonts.conf](file:///etc/fonts/fonts.conf)

Pràctiques
----------

**Molt important:** imprimeix i porta a classe l’XML Syntax Quick
Reference.

-   Instal·la si cal els editors `geany` i `gvim` (opcional).
-   Instal·la si cal els paquets auxiliars dels editors `geany` i
    `gedit`.
-   Consulta les dreceres de teclat de l’editor en la seva ajuda.
-   Confecciona una “xuleta” per les tecles CUA de moviment, selecció i
    us del clipboard.
-   Verifica que les tecles CUA funcionen igual en els editors `geany` i
    `gedit`.
-   Practica amb l’editor utilitzant una còpia del fitxer
    `/etc/fonts/fonts.conf`.

